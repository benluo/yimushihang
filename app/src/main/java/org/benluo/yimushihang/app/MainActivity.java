package org.benluo.yimushihang.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


public class MainActivity extends Activity implements View.OnClickListener {

//    File seletedFile;
    private Context ctx;
//    private int currentPosition = 0;
    private int currentSpeed = 12; //12 charactor per second
    private long loopTime = 1000 / currentSpeed;
    private InputStreamReader currentDisplayContent = null ;
    private Handler mHandler;
    private boolean runBtnStatus;
//    private long mStartTime;
    private TextView content;
    private Button bnStart;
    private Runnable mUpdateTimeTask;
    char[] currentDisplay = new char[3];
//    private final int REQUEST_CODE_PICK_DIR = 1;
    private final int REQUEST_CODE_PICK_FILE = 2;
//    private File readingFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        runBtnStatus = true;
        setContentView(R.layout.activity_main);
        content = (TextView) findViewById(R.id.tvContent);
        bnStart = (Button) findViewById(R.id.bnStart);
        content.setText(R.string.no_content);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_about:
                showAbout();
                return true;
            case R.id.action_select_article:
                selectFile();
                return true;
//            case R.id.action_settings:
//                settings();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAbout() {
        AlertDialog.Builder aboutDialogBuilder = new AlertDialog.Builder(ctx);
        String message = getString(R.string.about_content) + "\n" + getString(R.string.version) + getString(R.string.version_number);
        aboutDialogBuilder.setTitle(R.string.about)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();

    }

    private void selectFile(){
        Intent fileExploreIntent = new Intent(FileBrowserActivity.INTENT_ACTION_SELECT_FILE,
                null, this, FileBrowserActivity.class);
        fileExploreIntent.putExtra(FileBrowserActivity.showCannotReadParameter, false);
        startActivityForResult(fileExploreIntent,REQUEST_CODE_PICK_FILE);
    }

//    private void settings() {
//
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PICK_FILE && resultCode == RESULT_OK) {
            String fileNameWithWholePath = data.getStringExtra(FileBrowserActivity.returnFileParameter);
            FileInputStream in = null;
            try {
                in = new FileInputStream(new File(fileNameWithWholePath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                currentDisplayContent = new InputStreamReader(in, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // set up handler for runnable task
            mHandler = new Handler();
            mUpdateTimeTask = new Runnable() {
                public void run() {
                    try {
                        currentDisplayContent.read(currentDisplay, 0, 3);//  new int[]{currentPosition}, currentPosition + 3, currentDisplay, 0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    content.setText(String.valueOf(currentDisplay).replaceAll("(\r\n|\n\r|\r|\n)", "-"));
//                    currentPosition++;

                    mHandler.postDelayed(this, loopTime);
                }
            };
        }


    }

    @Override
    public void onClick(View v) {
//
//        new Timer().scheduleAtFixedRate(new TimerTask() {
//            //            private static int i=0;
//            @Override
//            public void run() {
//                char[] currentDisplay = new char[3];
//                TextView content = (TextView) findViewById(R.id.tvContent);
//                currentDisplayContent.getChars(currentPosition, currentPosition + 3, currentDisplay, 0);
//                content.setText("我是谁");//String.valueOf(currentDisplay));
//                currentPosition++;
//
//            }
//        }, 0, loopTime);//put here time 1000 milliseconds=1 second

        if (currentDisplayContent != null) {
            if (runBtnStatus) {
                mHandler.removeCallbacks(mUpdateTimeTask);
                mHandler.postDelayed(mUpdateTimeTask, loopTime);
                bnStart.setText(R.string.stop);
                runBtnStatus = false;
            } else {
                mHandler.removeCallbacks(mUpdateTimeTask);
                bnStart.setText(R.string.start);
                runBtnStatus = true;
            }

        }


//        running = window.setInterval(function(){
//            $('#display').text(((sentence.length > i)? sentence[i] : "") + ((sentence.length > i + 1)? sentence[i + 1] : "") + ((sentence.length > i + 2)? sentence[i + 2] : ""));
//            i = i + 1;
//            if (i >= sentence.length) {
//                reset(false);
//            }
//        }, 1000 / parseInt($('#speed').val()));
    }

//    void ToDBC(String txtstring) {
//        var tmp = "";
//        for(var i=0;i<txtstring.length;i++) {
//            if(txtstring.charCodeAt(i)==32) {
//                tmp= tmp+ String.fromCharCode(12288);
//            } else {
//                if(txtstring.charCodeAt(i)<127) {
//                    tmp=tmp+String.fromCharCode(txtstring.charCodeAt(i)+65248);
//                } else {
//                    tmp += txtstring[i];
//                }
//            }
//
//        }
//        return tmp;
//    }
}
